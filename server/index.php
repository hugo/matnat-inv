<!doctype html><?php
/*
 * Home page for application.
 *
 * Contains login form for non-logged in users, and for logged in
 * users contains links to remaining services.
 */
?><html>
<head>
	<meta charset="UTF-8"/>
<?php
require 'lib/init.php';
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>MatNats lagersystem</title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>
<h1>MatNats Lagersystem</h1>

<!-- TODO logout button -->
<!-- TODO my account button -->

<?php if (array_key_exists("error", $_SESSION)) { ?>
<div class="errmsg">
<?php 
/* Error is now printed. Remove it so next reload doesn't show it */
unset($_SESSION["error"]);
?>
</div>
<?php } ?>
<?php 
if (($user = validate_user())) { ?>
	Välkommen, <?php echo "${user['name']}"; ?>!
    <ul>
        <li><a href="/inventory.php">Inventarie</a></li>
        <li><a href="/current.php">För tillfället utlånat</a></li>
<?php if ($user['privilige'] === 'admin') { ?>
        <li><a href="/users.php">Hantera användare</a></li>
        <li><a href="/manage-inventory.php">Modifiera inventarie</a></li>
<?php } ?>
    </ul>
<?php } else { ?>
<form class="basic-form" action="/post/login.php" method="POST">
    <label for="username">Användarnamn:</label>
	<input id="username" name="username" placeholder="Användarnamn..." recuired="recuired" />

    <label for="password">Lösenord:</label>
	<input id="password" name="password" type="password" placeholder="Lösenord..." recuired="recuired" />

	<input type="submit" value="Logga in!" />
</form>
<?php } ?>

<?php
	/*
echo login_user("hugo", "incorrect");
echo "<br>";
echo login_user("hugo", "password");
echo "<br>";
echo login_user("boomknife", "password");
echo "<br>";
	 */
?>

</body>
</html>
