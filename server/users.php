<!doctype html><?php
/*
 * Page for administrating users.
 *
 * This page is only accessible for admins.
 * From here, the level of all users should be modifyable,
 * new users may be added, and old users removed.
 */
?><html>
<head>
	<meta charset="UTF-8"/>
<?php
require 'lib/init.php';
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>MatNats lagersystem</title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>
<h1>MatNats Lagersystem</h1>

<?php
?>

<h2>Användare</h2>
<!-- TODO test this form -->
<form>
<table>
	<thead>
		<tr>
			<th></th>
			<th>Användarnamn</th>
			<th>Namn</th>
			<th>Epost</th>
			<th>Privilegie</th>
		</tr>
	</thead>
	<tbody>
<?php
global $mysqli;
$stmt = $mysqli->prepare("
SELECT user.id
	 , username
	 , full_name
	 , email
	 , privilege AS type_id
     , user_types.title as type_name
FROM user
LEFT JOIN user_types ON user_types.id = user.privilege
");

$stmt->execute();
$stmt->bind_result($id, $username, $full_name, $email, $type_id, $type_name);
while ($stmt->fetch()) { ?>
<tr>
	<td><input type="checkbox" /></td>
	<td><?php echo $username; ?></td>
	<td><input value="<?php echo $full_name; ?>" name="<?php echo $username; ?>-name" /></td>
	<td><input value="<?php echo $email; ?>" name="<?php echo $username; ?>-email" type="email"/></td>
	<td>
		<select name="privilege">
<?php foreach ($usertype as $type) { ?>
<!-- TODO Select current role -->
<option value="<?php echo $type['id']; ?>" name=""><?php echo $type["name"]; ?></option>
<?php } ?>
		</select>
</td>
</tr>
<?php } ?>
	</tbody>
	<thead>
	</thead>
		<tr>
			<th></th>
			<th>Användarnamn</th>
			<th>Namn</th>
			<th>Epost</th>
			<th>Privilegie</th>
		</tr>
	</thead>
</table>
<input type="submit" value="Spara ändringar"/>
</form>

<!-- TODO test this form -->
<h2>Skapa ny användare</h2>
<form class="basic-form" action="/create_user_post.php" method="POST">
    <label for="username">Användarnamn:</label>
    <input id="username" name="username" required="required" placeholder="hugo"/>

    <label for="name">Fullständigt namn</label>
    <input id="name" name="name" required="required" placeholder="Hugo Hörnquist"/>


    <label for="password">Lösenord:</label>
    <input id="password" name="password" required="required" placeholder="Lösenord..." type="password" />

    <label for="email">Epostaddress:</label>
    <input id="email" name="email" type="email" required="required" placeholder="Epostaddress..."/>

    <label for="privilege">Privilegie</label>
    <select name="privilege" id="privilege">
    <?php
$result = $mysqli->query("SELECT id, title FROM user_types");
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <option value="<?php echo $row['id']; ?>"
><?php echo $row['title']; ?>
</option>
<?php
}
    ?>
    </select>

    <input type="submit" value="Skapa användare" />
<?php

?>
    </select>
</form>

</body>
</html>

