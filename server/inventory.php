<!doctype html><?php
/*
 * Primary page of the application.
 * Contains the table of inventory, which will be a form if you have
 * the rights to check in and out items.
 */
?><html>
<head>
	<meta charset="UTF-8"/>
<?php 
require 'lib/init.php';
if (! ($user = validate_user())) {
    die();
}
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title></title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>

<!-- TODO validate that user is logged in -->

<form>
<table>
    <thead>
        <tr>
            <th>Vara</th>
            <th>Lager</th>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
            <th>Ingående</th>
            <th>Utgående</th>
<?php } ?>
        </tr>
    </thead>
    <tbody>
<?php 
// $stmt->bind_param 

$stmt = $mysqli->prepare("
SELECT id, title, stock
FROM products
");

$stmt->execute();
$stmt->bind_result($id, $title, $stock);
while ($stmt->fetch()) { ?>
<tr>
    <td><?php echo $title; ?></td>
    <td><?php echo $stock; ?></td>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
    <td><input type="number" /></td>
    <td><input type="number" /></td>
<?php } ?>
</tr>
<?php } ?>
    </tbody>
    <thead>
        <tr>
            <th>Vara</th>
            <th>Lager</th>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
            <th>Ingående</th>
            <th>Utgående</th>
<?php } ?>
        </tr>
    </thead>
</table>
<input type="submit" value="Begär uttag" />
</form>

</body>
</html>
