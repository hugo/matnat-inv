<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);
error_reporting(~E_STRICT);

// $COOKIE_MATNAT_USER = "matnat_user";
$COOKIE_MATNAT_SESSION = "matnat_session";

session_start();

$mysqli = new mysqli("localhost", "matnat", "password", "matnat");

// TODO Only initialize database when needed. This is needlessly slow
// $handle = fopen("schema.sql", "r");
// $sql_data = fread($handle, filesize("schema.sql"));
// 
// $mysqli->multi_query($sql_data);


/**
 * Checks if the user is logged in.
 * 
 * Either returns 'false', or a record containing information about
 * the user, if it's logged in.
 */
function validate_user() {
    global $COOKIE_MATNAT_SESSION;
    global $mysqli;

    if (! array_key_exists($COOKIE_MATNAT_SESSION, $_COOKIE)) {
        error_log("No cookie for user");
        return false;
    }


    $stmt = $mysqli->prepare("
SELECT username, full_name, email, user_types.title
FROM session
LEFT JOIN user ON user.id = session.user
LEFT JOIN user_types ON user_types.id = user.privilege
WHERE session.expires > CURRENT_TIMESTAMP
  AND session_cookie = ?
");

    // error_log("Using cookie ${_COOKIE[$COOKIE_MATNAT_SESSION]}");
	$stmt->bind_param("s", $_COOKIE[$COOKIE_MATNAT_SESSION]);

    $stmt->execute();

    $stmt->bind_result($username, $name, $email, $privilige);

    if (! $stmt->fetch()) {
        // error_log("User not authenticated");
        return false;
    }

    // error_log("User ${username} IS authenticated");

    return array (
        'username'  => $username,
        'name'      => $name,
        'email'     => $email,
        'privilige' => $privilige,
    );
}

/**
 * Attempts to login a user.
 *
 * On failure false is returned, while on success true is returned.
 */
function login_user($username, $password) {
    global $COOKIE_MATNAT_SESSION;
    global $mysqli;

    $stmt = $mysqli->prepare("
SELECT user.id, hash_method.name, salt, hash
FROM user
LEFT JOIN hash_method ON hash_method.id = hash_method
WHERE username = ?
");

    $stmt->bind_param("s", $username);
    $stmt->execute();

    $stmt->bind_result($user_id, $hashmethod, $salt, $refhash);

    if (! $stmt->fetch()) {
        /* Incorrect username */
        // $_SESSION["error"] = "Felaktigt lösenord eller användarnamn.";
        $_SESSION["error"] = "Okänt användarnamn";
        return false;
    }

    switch ($hashmethod) {
    case 'md5':
        $hash = md5($salt . $password);
        break;
    case 'sha256':
        $hash = sha256($salt . $password);
        break;
    default:
        // TODO possibly better trace
        error_log("Unknown hash method: $hashmethod");
        $_SESSION["error"] = "Udda pyttipanna ($hashmethod). Kontakta driftsanvsarig";
        return false;
    }

    if ($hash !== $refhash) {
        /* Incorrect username */
        // $_SESSION["error"] = "Felaktigt lösenord eller användarnamn.";
        $_SESSION["error"] = "Felaktigt lösenord";
        return false;
    }

    $stmt->free_result();

    $stmt = $mysqli->prepare("
INSERT INTO session (session_cookie, expires, user)
WITH end_time (end) AS (SELECT date_add(CURRENT_TIMESTAMP, INTERVAL 1 DAY))
SELECT uuid(), end_time.end, ? FROM end_time
ON DUPLICATE KEY UPDATE expires = end_time.end
RETURNING session_cookie, unix_timestamp(expires)
");

    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $stmt->bind_result($cookie, $expires);

    if (! $stmt->fetch()) {
        // TODO HTTP 500 series
        die();
        return false;
    }

    error_log("cookie = $cookie");

    setcookie(
        $COOKIE_MATNAT_SESSION,
        $cookie,
        $expires,
        "/",
    );

    error_log("Cookie sat");

	return true;
}
